/*
 * cqueue.h
 *
 *  Created on: 31.07.2016
 *      Author: tetraquark | tetraquark.ru
 */

#ifndef INCLUDE_CQUEUE_H_
#define INCLUDE_CQUEUE_H_

#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>

#define USING_MUTEX // for pthread mutex

#define QUEUE_SIZE_TYPE uint32_t

typedef enum{
	FN_OK = 0,
	FN_FAIL
} fn_result_t;

typedef struct cqueue_node{
	void* data;
	struct cqueue_node* next;
} cqueue_node_t;

typedef struct{
	cqueue_node_t* start_elem;
	cqueue_node_t* end_elem;
	QUEUE_SIZE_TYPE size;
	size_t element_size;
#ifdef USING_MUTEX
	pthread_mutex_t *mutex;
#endif
} cqueue;

fn_result_t cqueue_init(cqueue* __q, size_t __elem_size);
fn_result_t cqueue_clear(cqueue* __q);
fn_result_t cqueue_enqueue(cqueue* __q, void* __data);
void* cqueue_dequeue(cqueue* __q);
QUEUE_SIZE_TYPE cqueue_size(cqueue* __q);

#ifdef USING_MUTEX
fn_result_t cqueue_free(cqueue* __q);
#endif

#endif /* INCLUDE_CQUEUE_H_ */
