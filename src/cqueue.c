/*
 * cqueue.c
 *
 *  Created on: 31.07.2016
 *      Author: tetraquark | tetraquark.ru
 */

#include "../include/cqueue.h"

fn_result_t cqueue_init(cqueue* __q, size_t __elem_size){
	if(__q == NULL)
		return FN_FAIL;

#ifdef USING_MUTEX
	__q->mutex = (pthread_mutex_t*) malloc(sizeof(pthread_mutex_t));
	if(__q->mutex == NULL)
		return FN_FAIL;
	if(pthread_mutex_init(__q->mutex, NULL) != 0){
		free(__q->mutex);
		__q->mutex = NULL;
		return FN_FAIL;
	}
#endif

	__q->size = 0;
	__q->end_elem = __q->start_elem = NULL;
	__q->element_size = __elem_size;
	return FN_OK;
}

#ifdef USING_MUTEX
fn_result_t cqueue_free(cqueue* __q){
	if(__q == NULL || __q->mutex == NULL)
		return FN_FAIL;

	if(pthread_mutex_destroy(__q->mutex) != 0)
		return FN_FAIL;

	free(__q->mutex);
	__q->mutex = NULL;

	return FN_OK;
}
#endif

fn_result_t cqueue_clear(cqueue* __q){
	if(__q == NULL)
		return FN_FAIL;

#ifdef USING_MUTEX
	if(__q->mutex == NULL)
		return FN_FAIL;
	pthread_mutex_lock(__q->mutex);
#endif
	QUEUE_SIZE_TYPE i = 0;
	cqueue_node_t* curr = __q->start_elem;
	cqueue_node_t* next = NULL;
	for(i = 0; i < __q->size; i++){
		next = curr->next;
		curr->next = NULL;
		free(curr->data);
		free(curr);
		curr = next;
	}

	__q->end_elem = __q->start_elem = NULL;
	__q->size = 0;
#ifdef USING_MUTEX
	pthread_mutex_unlock(__q->mutex);
#endif

	return FN_OK;
}

fn_result_t cqueue_enqueue(cqueue* __q, void* __data){
	if(__q == NULL)
		return FN_FAIL;

#ifdef USING_MUTEX
	if(__q->mutex == NULL)
		return FN_FAIL;
#endif
	cqueue_node_t* new_node = (cqueue_node_t*) malloc(sizeof(cqueue_node_t));
	if(new_node == NULL)
		return FN_FAIL;

	new_node->next = NULL;
	new_node->data = malloc(__q->element_size);
	if(new_node->data == NULL){
		free(new_node);
		return FN_FAIL;
	}
	memcpy(new_node->data, __data, __q->element_size);

#ifdef USING_MUTEX
	pthread_mutex_lock(__q->mutex);
#endif
	if(__q->size <= 0){
		__q->start_elem = new_node;
		__q->end_elem = new_node;
	}
	else if(__q->size == 1){
		__q->start_elem->next = new_node;
		__q->end_elem = new_node;
	}
	else{
		__q->end_elem->next = new_node;
		__q->end_elem = new_node;
	}
	__q->size++;
#ifdef USING_MUTEX
	pthread_mutex_unlock(__q->mutex);
#endif
	return FN_OK;
}

void* cqueue_dequeue(cqueue* __q){
	if(__q == NULL || __q->size == 0)
		return NULL;

#ifdef USING_MUTEX
	if(__q->mutex == NULL)
		return NULL;
#endif

	void* ret_data_ptr = malloc(__q->element_size);
	if(ret_data_ptr == NULL)
		return NULL;

#ifdef USING_MUTEX
	pthread_mutex_lock(__q->mutex);
#endif
	memcpy(ret_data_ptr, __q->start_elem->data, __q->element_size);

	if(__q->size == 1){
		free(__q->start_elem->data);
		free(__q->start_elem);
		__q->start_elem = NULL;
		__q->end_elem = NULL;
	}
	else{
		cqueue_node_t* tmp_ptr = __q->start_elem;
		__q->start_elem = __q->start_elem->next;
		free(tmp_ptr->data);
		free(tmp_ptr);
	}
	__q->size--;
#ifdef USING_MUTEX
	pthread_mutex_unlock(__q->mutex);
#endif
	return ret_data_ptr;
}

QUEUE_SIZE_TYPE cqueue_size(cqueue* __q){
	QUEUE_SIZE_TYPE size = 0;
	if(__q == NULL)
		return size;

#ifdef USING_MUTEX
	if(__q->mutex == NULL)
		return size;
	pthread_mutex_lock(__q->mutex);
#endif

	size = __q->size;

#ifdef USING_MUTEX
	pthread_mutex_unlock(__q->mutex);
#endif
	return size;
}
